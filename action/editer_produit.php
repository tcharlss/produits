<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Point d'entrée d'édition d'un produit
 *
 * On ne peut entrer que par un appel en fournissant $id_produit
 * ou avec un argument d'action sécurisée de type "id_produit"
 * 
 * @param string|null $arg
 * @param array $set
 * @return array
 */
function action_editer_produit_dist($id_produit = null, $set = null) {
	include_spip('inc/autoriser');
	$err = '';
	if (is_null($id_produit)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$id_produit = $securiser_action();
	}

	// appel incorrect ou depuis une url erronnée interdit
	if (empty($id_produit)) {
		include_spip('inc/minipres');
		echo minipres(_T('info_acces_interdit'));
		die();
	}

	// si id_produit n'est pas un nombre, c'est une création
	if (!$id_produit = intval($id_produit)) {
		$parent     = _request('parent');
		$id_produit = produit_inserer($parent);
	}

	if (!($id_produit = intval($id_produit)) > 0) {
		return array($id_produit, _L('produit : echec enregistrement en base'));
	}

	// Enregistre l'envoi dans la BD
	$err = produit_modifier($id_produit, $set);

	return array($id_produit,$err);
}

/**
 * Crée un nouveau produit et retourne son ID
 *
 * @param string $parent
 *     Parent de la forme `objetN`
 * @param array $set
 *     Un tableau avec les champs par défaut lors de l'insertion
 * @return int
 *     Identifiant du nouvel produit
 */
function produit_inserer($parent, $set = null) {

	include_spip('inc/config');

	$champs = array();
	$config = lire_config('produits');

	// Décoder le parent
	$objet    = '';
	$id_objet = 0;
	if ($parent = produits_decoder_parent($parent)) {
		list($objet, $id_objet) = $parent;
	}

	// Si la configuration limite l’ajout à un secteur,
	// on force une rubrique comme parent en prenant le premier secteur défini.
	if (!empty($config['limiter_secteur']) and is_array($config['limiter_secteur'])) {
		$objet    = 'rubrique';
		$id_objet = intval($config['limiter_secteur'][0]);
	}

	// Si on a un parent, retrouver sa langue et son secteur
	if ($objet and $id_objet) {
		include_spip('base/objets');
		$trouver_table  = charger_fonction('trouver_table', 'base');
		$objet          = objet_type($objet);
		$table_objet    = table_objet_sql($objet);
		$cle_objet      = id_table_objet($objet);
		$desc_objet     = $trouver_table($table_objet);

		if ($select = array_intersect(array_keys($desc_objet['field']), array('lang','id_secteur'))) {
			$row_parent = sql_fetsel($select, $table_objet, $cle_objet.'='.intval($id_objet));
			$champs = array_merge($champs, $row_parent);
		}

		$champs['objet']    = $objet;
		$champs['id_objet'] = $id_objet;
	}

	// Langue : si les liens de traduction sont autorisés dans le parent,
	// on essaie avec la langue de l'auteur ou à défaut celle du parent.
	// Sinon c'est la langue du parent qui est choisie + heritée.
	if (
		isset($desc_objet['field']['lang'])
		and !empty($GLOBALS['meta']['multi_objets'])
		and in_array($table_objet, explode(',', $GLOBALS['meta']['multi_objets']))
	) {
		lang_select($GLOBALS['visiteur_session']['lang']);
		if (in_array($GLOBALS['spip_lang'], explode(',', $GLOBALS['meta']['langues_multilingue']))) {
			$champs['lang'] = $GLOBALS['spip_lang'];
			if (isset($desc_objet['field']['langue_choisie'])) {
				$champs['langue_choisie'] = 'oui';
			}
		}
	} elseif (isset($desc_objet['field']['lang']) and isset($desc_objet['field']['langue_choisie'])) {
		$champs['lang'] = (isset($row_parent['lang']) ? $row_parent['lang'] : $GLOBALS['meta']['langue_site']);
		$champs['langue_choisie'] = 'non';
	}

	// La date de tout de suite
	$champs['date'] = date('Y-m-d H:i:s');

	// Le statut en cours de redac
	$champs['statut'] = 'prepa';

	if ($set) {
		$champs = array_merge($champs, $set);
	}

	// Envoyer aux plugins avant insertion
	$champs = pipeline(
		'pre_insertion',
		array(
			'args' => array(
				'table'  => 'spip_produits',
				'parent' => $parent,
			),
			'data' => $champs
		)
	);

	// Insérer l'objet
	$id_produit = sql_insertq('spip_produits', $champs);

	if ($id_produit > 0) {
		// contrôler si le serveur n'a pas renvoyé une erreur et associer l'auteur
		// si le form n'a pas posté un id_auteur (même vide, ce qui sert a annuler cette auto association)
		$id_auteur = ((is_null(_request('id_auteur')) and isset($GLOBALS['visiteur_session']['id_auteur'])) ?
			$GLOBALS['visiteur_session']['id_auteur']
			: _request('id_auteur'));
		if ($id_auteur) {
			include_spip('action/editer_auteur');
			auteur_associer($id_auteur, array('produit' => $id_produit));
		}
	}

	// Envoyer aux plugins après insertion
	pipeline(
		'post_insertion',
		array(
			'args' => array(
				'table'    => 'spip_produits',
				'id_objet' => $id_produit,
				'parent'   => $parent,
			),
			'data' => $champs
		)
	);

	return $id_produit;
}

/**
 * Appelle la fonction de modification d'un produit
 *
 * @param int $id_produit
 * @param array $set
 * @return string
 */
function produit_modifier($id_produit, $set = null) {
	$err = '';

	include_spip('inc/modifier');
	include_spip('inc/filtres');
	$c = collecter_requests(
		// white list
		objet_info('produit', 'champs_editables'),
		// black list = champs modifiés dans instituer
		array('date', 'statut', 'parent', 'id_secteur'),
		// données eventuellement fournies
		$set
	);

	// Si le produit est publié, invalider les caches et demander sa réindexation
	if (objet_test_si_publie('produit', $id_produit)) {
		$invalideur = "id='produit/$id_produit'";
		$indexation = true;
	} else {
		$invalideur = '';
		$indexation = false;
	}

	$err = objet_modifier_champs(
		'produit',
		$id_produit,
		array(
			'data'       => $set,
			'nonvide'    => array('titre' => _T('info_sans_titre')),
			'invalideur' => $invalideur,
			'indexation' => $indexation,
		),
		$c
	);
	if ($err) {
		return $err;
	}

	// Modification du statut, de la date ou du parent
	$requests = collecter_requests(array('date', 'statut', 'parent'), array(), $set);
	$err = produit_instituer($id_produit, $requests);

	return $err;
}


/**
 * Modifie le statut, la date ou le parent d'un produit
 *
 * @param int $id_produit
 *     Numéro du produit
 * @param array $c
 *     Champs possiblement modifiés issus de collecter_requests()
 * @param bool $calcul_rub
 *     Changer le statut des rubriques concernées
 * @return string
 */
function produit_instituer($id_produit, $c, $calcul_rub = true){
	include_spip('inc/autoriser');
	include_spip('inc/rubriques');
	include_spip('inc/modifier');

	$row           = sql_fetsel('statut, date, objet, id_objet', 'spip_produits', 'id_produit='.intval($id_produit));
	$objet         = $row['objet'];
	$id_objet      = $row['id_objet'];
	$statut_ancien = $statut = $row['statut'];
	$date_ancienne = $date = $row['date'];
	$champs        = array();

	$d = isset($c['date']) ? $c['date'] : null;
	$s = isset($c['statut']) ? $c['statut'] : $statut;

	// On ne modifie le statut que si c'est autorisé
	if ($s != $statut or ($d and $d != $date)) {
		if ($objet == 'rubrique' and autoriser('publierdans', 'rubrique', $id_objet)) {
			$statut = $champs['statut'] = $s;
		} elseif (autoriser('modifier', 'produit', $id_produit) and $s != 'publie') {
			$statut = $champs['statut'] = $s;
		} else {
			spip_log("editer_produit $id_produit refus " . join(' ', $c));
		}

		// En cas de publication, fixer la date à "maintenant"
		// sauf si $c commande autre chose
		// ou si le produit est déjà daté dans le futur
		// En cas de proposition d'un produit (mais pas dépublication), idem
		if (
			$champs['statut'] == 'publie'
			or (
				$champs['statut'] == 'prop'
				and ($d or !in_array($statut_ancien, array('publie', 'prop')))
			)
		) {
			if ($d or strtotime($d = $date) > time()) {
				$champs['date'] = $date = $d;
			} else {
				$champs['date'] = $date = date('Y-m-d H:i:s');
			}
		}
	}

	// Verifier que le nouveau parent existe et qu'il est différent du parent actuel
	if (
		isset($c['parent'])
		and list($objet_nouveau, $id_objet_nouveau) = produits_decoder_parent($c['parent'])
		and ($objet_nouveau != $objet or $id_objet_nouveau != $id_objet)
	) {
		$champs['objet']    = $objet_nouveau;
		$champs['id_objet'] = $id_objet_nouveau;

		// Si le produit était publié et que le nouveau parent est une rubrique
		// dont le demandeur n'est pas admin, repasser en statut 'proposé'.
		if (
			$objet_nouveau == 'rubrique'
			and $statut == 'publie'
			and !autoriser('publierdans', 'rubrique', $id_objet_nouveau)
		) {
			$champs['statut'] = 'prop';
		}
	}

	// Envoyer aux plugins
	$champs = pipeline(
		'pre_edition',
		array(
			'args' => array(
				'table'         => 'spip_produits',
				'id_objet'      => $id_produit,
				'action'        => 'instituer',
				'statut_ancien' => $statut_ancien,
			),
			'data' => $champs
		)
	);

	if (!count($champs)) {
		return;
	}

	// Envoyer les modifications et calculer les héritages
	editer_produit_heritage($id_produit, $objet, $id_objet, $statut_ancien, $champs, $calcul_rub);

	// Invalider les caches
	include_spip('inc/invalideur');
	suivre_invalideur("id='id_produit/$id_produit'");

	/*
	if ($date) {
		$t = strtotime($date);
		$p = @$GLOBALS['meta']['date_prochain_postdate'];
		if ($t > time() and (!$p or ($t < $p))) {
			ecrire_meta('date_prochain_postdate', $t);
		}
	}
	*/

	// Pipeline
	pipeline(
		'post_edition',
		array(
			'args' => array(
				'table'         => 'spip_produits',
				'id_objet'      => $id_produit,
				'action'        => 'instituer',
				'statut_ancien' => $statut_ancien,
			),
			'data' => $champs
		)
	);

	// Notifications
	if ($notifications = charger_fonction('notifications', 'inc', true)) {
		$notifications(
			'produit_instituer',
			$id_produit,
			array('statut' => $statut, 'statut_ancien' => $statut_ancien, 'date'=>$date)
		);
	}

	return '';
}

/**
 * Fabrique la requete d'institution du produit, avec champs herités
 *
 * @param int $id_produit
 *     Numéro du produit
 * @param string $objet
 *     Ancien objet parent
 * @param int $id_objet
 *     Ancien numéro du parent
 * @param string $statut
 *     Ancien statut
 * @param array $champs
 *     Champs modifiés
 * @param boolean $cond
 *     Changer le statut des rubriques concernées
 * @return void
 */
function editer_produit_heritage($id_produit, $objet, $id_objet, $statut, $champs, $cond = true) {

	// Si on déplace le produit, changer aussi sa langue (si héritée) et son secteur
	if (
		isset($champs['objet'])
		and isset($champs['id_objet'])
	) {

		$trouver_table = charger_fonction('trouver_table', 'base');
		$objet         = $champs['objet'];
		$id_objet      = $champs['id_objet'];
		$table_objet   = table_objet_sql($objet);
		$cle_objet     = id_table_objet($objet);
		$desc_objet    = $trouver_table($table_objet);

		if ($select = array_intersect(array_keys($desc_objet['field']), array('lang','langue_choisie','id_secteur'))) {
			$row_parent = sql_fetsel($select, $table_objet, $cle_objet.'='.intval($id_objet));
			if (isset($row_parent['id_secteur'])) {
				$champs['id_secteur'] = $row_parent['id_secteur'];
			}
			if (
				isset($row_parent['lang'])
				and isset($row_parent['langue_choisie'])
			) {
				if (sql_countsel($table_objet, $cle_objet.'='.intval($id_objet) . " AND langue_choisie<>'oui' AND lang<>" . sql_quote($row_parent['lang']))) {
					$champs['lang'] = $row_parent['lang'];
				}
			}
		}
	}

	if (!$champs) {
		return;
	}

	sql_updateq('spip_produits', $champs, 'id_produit='.intval($id_produit));

	// Changer le statut des rubriques concernées
	if ($cond and $objet == 'rubrique') {
		include_spip('inc/rubriques');
		//$postdate = ($GLOBALS['meta']['post_dates'] == 'non' and isset($champs['date']) and (strtotime($champs['date']) < time())) ? $champs['date'] : false;
		$postdate = false;
		calculer_rubriques_if($id_objet, $champs, $statut, $postdate);
	}
}
