<?php
/**
 * Action non sécurisée pour convertir un montant HT en TTC et inversement
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function action_produits_convertir_montant_dist() {

	$nouveau_montant = null;
	$montant         = _request('amount');
	$type            = _request('type'); // ht ou ttc
	$taxe            = _request('taxRate');

	if ($montant) {
		include_spip('produits_fonctions');
		switch ($type) {
			case 'ht':
			case 'notax':
				$nouveau_montant = produits_montant_ht_vers_ttc($montant, $taxe);
				break;
			case 'ttc':
			case 'tax':
				$nouveau_montant = produits_montant_ttc_vers_ht($montant, $taxe);
				break;
		}
	}

	echo $nouveau_montant;
}