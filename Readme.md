# Produits

![](prive/themes/spip/images/produits.svg)

Ceci est un fork de https://git.spip.net/plugin/produits pour tester des choses, notamment :

* Parents : le produits peuvent avoir n’importe quel type de parent, pas uniquement des rubriques.
* Édition du prix HT/TTC simplifiée
* Simplification du code en certains endroits, ménage

On verra après si les évolutions peuvent être reversées dans le plugin d'origine.

> Il n'y a pour l'instant pas de fonction de migration de la v1 originelle vers cette v2, à tester donc uniquement pour une nouvelle install.