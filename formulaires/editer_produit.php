<?php
/**
 * Gestion du formulaire de d'édition de produit
 *
 * @plugin     Produits
 * @copyright  2015
 * @author     Les Développements Durables
 * @licence    GNU/GPL
 * @package    SPIP\Produits\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/config');
include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Liste des saisies du formulaire
 *
 * @param int|string $id_produit
 *     Identifiant du produit. 'new' pour un nouveau produit.
 * @param string $parent
 *     Alias du parent sous la forme `objetN` : `rubrique5`, `article10`...
 * @param string $type
 *     Type de produit (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un lot source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du lot, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_produit_saisies_dist($id_produit = 'new', $parent = '', $type = '', $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {

	include_spip('inc/config');
	$config          = lire_config('produits');
	$taxe_defaut     = lire_config('produits/taxe', 0);
	$parent_readonly = (isset($config['limiter_secteur']) ? count($config['limiter_secteur']) === 1 : false);
	$infos_parent    = recuperer_fond('saisies/inc-produits_parent', array('id_produit' => $id_produit, 'parent' => $parent));

	$saisies = array(
		'options' => array(),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom'         => 'titre',
				'label'       => _T('produits:produit_champ_titre_label'),
				'obligatoire' => 'oui',
				'placeholder' => _T('info_sans_titre'),
			)
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom'           => 'parent',
				'label'         => _T('produits:produit_champ_parent_label'),
				'inserer_debut' => $infos_parent,
				'explication'   => _T('produits:produit_champ_parent_explication'),
				'placeholder'   => _T('produits:produit_champ_parent_placeholder'),
				'readonly'      => $parent_readonly,
				'attributs'     => 'data-selecteur="generique" data-select-php="oui" data-select-params="'.attribut_html(json_encode(array('objets_exclus' => array('produits', 'plugins')), true)).'"',
			),
		),
		array(
			'saisie' => 'hidden',
			'options' => array(
				'nom' => 'type',
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom'   => 'reference',
				'label' => _T('produits:produit_champ_reference_label'),
			)
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom'         => 'prix_ht',
				'label'       => _T('produits:produit_champ_prix_ht_label'),
				'obligatoire' => 'oui',
				'type'        => 'number',
				'step'        => 0.01,
				'class'       => 'montant',
				'inserer_fin' =>
					'<link href="' . find_in_path('lib/editpricehelper/dist/editpricehelper.min.css') .'" rel="stylesheet" type="text/css">'
					. '<script src="' . find_in_path('lib/editpricehelper/dist/jquery.editpricehelper.min.js') .'"></script>'
					. '<script src="' . produire_fond_statique('prive/javascript/produits_editer.js') .'"></script>'
			),
			'verifier' => array(
				'type' => 'decimal',
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom'         => 'taxe',
				'label'       => _T('produits:produit_champ_taxe_label'),
				'explication' => _T(
					'produits:produit_champ_taxe_explication',
					array('taxe' => filtrer('produits_afficher_taxe', $taxe_defaut))
				),
				'type'        => 'number',
				'min'         => 0,
				'max'         => 1,
				'step'        => 0.01,
				'class'       => 'montant',
				// 'inserer_fin' => '<span class="pourcent">&nbsp;&#37;</span>',
			),
			'verifier' => array(
				'type' => 'decimal'
			)
		),
		array(
			'saisie' => 'textarea',
			'options' => array(
				'nom'   => 'descriptif',
				'label' => _T('produits:produit_champ_descriptif_label'),
				'rows'  => 4
			)
		),
		array(
			'saisie' => 'textarea',
			'options' => array(
				'nom'   => 'texte',
				'label' => _T('produits:produit_champ_texte_label'),
				'rows'  => 8
			)
		),
		array(
			'saisie' => 'case',
			'options' => array(
				'nom' => 'immateriel',
				'label_case'      => _T('produits:produit_champ_immateriel_label'),
				'conteneur_class' => 'pleine_largeur',
				'valeur_oui'      => '1'
			)
		),
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom'         => 'conditionnement',
				'label'       => _T('produits:legend_dimensions'),
				'afficher_si' => '@immateriel@ != "1"'
			),
			'saisies' => array(
				array(
					'saisie' => 'input',
					'options' => array(
						'nom'   => 'poids',
						'label' => _T('produits:produit_champ_poids_label'),
					),
					'verifier' => array(
						'type' => 'decimal'
					)
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom'   => 'largeur',
						'label' => _T('produits:produit_champ_largeur_label'),
					)
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom'   => 'longueur',
						'label' => _T('produits:produit_champ_longueur_label'),
					)
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom'   => 'hauteur',
						'label' => _T('produits:produit_champ_hauteur_label'),
					)
				)
			)
		)
	);

	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité
 *
 * @param int|string $id_produit
 *     Identifiant du produit. 'new' pour un nouveau produit.
 * @param string $parent
 *     Alias du parent sous la forme `objetN` : `rubrique5`, `article10`...
 * @param string $type
 *     Type de produit (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un lot source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du lot, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_produit_identifier_dist($id_produit = 'new', $parent = '', $type = '', $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	return serialize(array(intval($id_produit), $parent, $type));
}

/**
 * Chargement du formulaire d'édition de produit
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_produit
 *     Identifiant du produit. 'new' pour un nouveau produit.
 * @param string $parent
 *     Alias du parent sous la forme `objetN` : `rubrique5`, `article10`...
 * @param string $type
 *     Type de produit (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un lot source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du lot, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Environnement du formulaire
 */
function formulaires_editer_produit_charger($id_produit = 'new', $parent = '', $type = '', $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {

	// Chargement générique
	$contexte = formulaires_editer_objet_charger('produit', $id_produit, $parent, $lier_trad, $retour, $config_fonc, $row, $hidden);

	// En cas de création, on remet le parent dans le contexte à la main
	// car formulaires_editer_objet_charger ignore tout ce qui n'est pas numérique.
	// Pour un objet existant, ça se fait dans precharger.
	if (!intval($id_produit)) {
		// Avec limitation à une seule rubrique, on force celle-ci en parent.
		// La saisie est alors en readonly.
		if (
			$config = lire_config('produits')
			and is_array($config['limiter_secteur'])
			and (count($config['limiter_secteur']) === 1)
		) {
			$parent = 'rubrique'.$config['limiter_secteur'][0];
		}
		$contexte['parent'] = $parent;
	}

	return $contexte;
}

/**
 * Vérifications du formulaire d'édition de produit
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_produit
 *     Identifiant du produit. 'new' pour un nouveau produit.
 * @param string $parent
 *     Alias du parent sous la forme `objetN` : `rubrique5`, `article10`...
 * @param string $type
 *     Type de produit (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un lot source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du lot, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Tableau des erreurs
 */
function formulaires_editer_produit_verifier($id_produit = 'new', $parent = '', $type = '', $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {

	include_spip('base/objets');
	$erreurs           = array();
	$config            = lire_config('produits');
	list($objet, $id_objet) = produits_decoder_parent(_request('parent'));
	$has_parent        = ($objet and $id_objet);
	$is_nouveau_parent = ($parent != _request('parent'));
	$table_objet       = table_objet_sql($objet);
	$cle_objet         = id_table_objet($objet);

	// Normaliser la taxe : ne pas enregistrer "0" si elle est vide
	if (!_request('taxe')) {
		set_request('taxe', null);
	}

	// Si nouveau parent, vérifier qu'il existe
	if (
		$has_parent
		and $is_nouveau_parent
		and !intval(sql_countsel($table_objet, $cle_objet.'=' . intval($id_objet)))
	) {
		$erreurs['parent'] = _T('produits:erreur_parent_inexistant');
	// Si nouveau parent, vérifier qu'il est dans le bon secteur si la limitation est activée
	} elseif (
		$has_parent
		and $is_nouveau_parent
		and !empty($config['limiter_secteur'])
	) {
		include_spip('inc/filtres');
		$objet_champs = objet_info($objet, 'field');
		// Soit le parent n'est pas rangeable dans un secteur
		if (empty($objet_champs['id_secteur'])) {
			$erreurs['parent'] = _T('produits:erreur_parent_sans_id_secteur', array('objet' => _T(objet_info($objet, 'texte_objet'))));
		// Soit il est dans un secteur non autorisé
		} else {
			$id_secteur = sql_getfetsel('id_secteur', $table_objet, $cle_objet.'='.intval($id_objet));
			if (!in_array($id_secteur, $config['limiter_secteur'])) {
				$titres = sql_allfetsel('titre', 'spip_rubriques', sql_in('id_rubrique', $config['limiter_secteur']));
				$titres = join('/', array_column($titres, 'titre'));
				$erreurs['parent'] .= _T('produits:erreur_secteurs_autorises', array('secteurs' => $titres));
			}
		}
	}

	// Vérification générique
	$erreurs += formulaires_editer_objet_verifier('produit', $id_produit, array('titre'));

	// $erreurs['message_erreur'] = 'stop!';

	return $erreurs ;
}

/**
 * Traitement du formulaire d'édition de produit
 *
 * Traiter les champs postés
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_produit
 *     Identifiant du produit. 'new' pour un nouveau produit.
 * @param string $parent
 *     Alias du parent sous la forme `objetN` : `rubrique5`, `article10`...
 * @param string $type
 *     Type de produit (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un lot source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du lot, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Retours des traitements
 */
function formulaires_editer_produit_traiter($id_produit = 'new', $parent = '', $type = '', $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$retours = formulaires_editer_objet_traiter('produit', $id_produit, $parent, $lier_trad, $retour, $config_fonc, $row, $hidden);
	return $retours;
}