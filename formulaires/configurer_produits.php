<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_configurer_produits_saisies_dist(){
	include_spip('inc/config');
	$config = lire_config('produits') ;
	return array(
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'taxe',
				'label' => _T('produits:configurer_taxe_defaut_label'),
				'explication' => _T('produits:configurer_taxe_defaut_explication'),
				'defaut' => $config['taxe'],
			),
			'verifier' => array(
				'type' => 'decimal'
			)
		),
		array(
			'saisie' => 'choisir_objets',
			'options' => array(
				'nom' => 'objets',
				'label' => _T('produits:configurer_objets_label'),
				'defaut' => $config['objets'],
				'exclus' => array('spip_produits'),
			),
		),
		// array(
		// 	'saisie' => 'oui_non',
		// 	'options' => array(
		// 		'nom' => 'editer_ttc',
		// 		'label' => _T('produits:configurer_editer_ttc_label'),
		// 		'explication' => _T('produits:configurer_editer_ttc_explication'),
		// 		'defaut' => $config['editer_ttc'],
		// 	)
		// ),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'precision_ttc',
				'label' => _T('produits:configurer_precision_ttc_label'),
				'explication' => _T('produits:configurer_precision_ttc_explication'),
				'defaut' => $config['precision_ttc'],
			),
			'verifier' => array(
				'type' => 'entier',
				'options' => array(
					'min' => 1,
					'max' => 6,
				),
			)
		),
		array(
			'saisie' => 'secteur',
			'options' => array(
				'nom' => 'limiter_secteur',
				'label' => _T('produits:configurer_limiter_secteur_label'),
				'explication' => _T('produits:configurer_limiter_secteur_explication'),
				'multiple' => 'oui',
				'defaut' => $config['limiter_secteur'],
			)
		),
		// array(
		// 	'saisie' => 'case',
		// 	'options' => array(
		// 		'nom' => 'publier_rubriques',
		// 		'label' => _T('produits:publier_rubriques_label'),
		// 		'label_case' => _T('produits:publier_rubriques_label_case'),
		// 		'explication' => _T('produits:publier_rubriques_explication'),
		// 		'defaut' => $config['publier_rubriques'],
		// 	)
		// ),
	);
}
