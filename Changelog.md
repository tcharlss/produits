# Changelog

## v2

* Pipeline `pre_boucle` : supprimé car inutile (et inutilisé)
* Pipeline `optimiser_base_disparus` : suppression du nettoyage de la table de liens (puisqu'il n'y en a pas...)
* Configuration : ajout de la saisie des objets
* Configuration : renommage de la clé `limiter_ident_secteur` en `limiter_secteur` et suppression de `limiter_ajout` qui est redondant et complique inutilement le code.
* Configuration : désativation de l'option `Publier les rubriques`, c'est fait tout le temps dans `action/editer_produit`.
* Désactivation de certains pipelines.
* Utilisation du script « Edit Price Helper » pour l'édition du prix.

**TODO**

- [ ] Migration