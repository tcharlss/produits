<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Renvoie le prix TTC d'un produit à partir de son prix HT
 *
 * @param int $id_produit
 * @param float $prix_ht
 * @return float
 */
function prix_produit_dist($id_produit, $prix_ht) {
	$prix = floatval($prix_ht);
	$id_produit = intval($id_produit);

	// On applique en priorité la taxe définie explicitement dans le produit
	if (
		$id_produit > 0
		and include_spip('base/abstract_sql')
		and (is_numeric($taxe = sql_getfetsel('taxe', 'spip_produits', 'id_produit = '.$id_produit)))
	){
		$prix += $prix * floatval($taxe);
	}
	// Sinon on prend la taxe par défaut
	else {
		include_spip('inc/config');
		$prix += $prix * floatval(lire_config('produits/taxe', 0));
	}

	return $prix;
}