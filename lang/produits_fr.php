<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/produits/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configurer_editer_ttc_explication' => 'Les prix des produits sont éditables en TTC et non plus en HT',
	'configurer_editer_ttc_label' => 'Editer les produits en TTC',
	'configurer_objets_label' => 'Contenus sur lesquels activer la gestion des produits.',
	'configurer_precision_ttc_explication' => 'Indiquer la précision d’arrondi après la virgule pour les prix TTC (par défaut 2)',
	'configurer_precision_ttc_label' => 'Précision sur les prix TTC',
	'configurer_limiter_secteur_explication' => 'Restreindre l’ajout de produits à un ou plusieurs secteurs (exclut d’office les contenus sans rangement par secteur).',
	'configurer_limiter_secteur_label' => 'Limiter',
	'configurer_taxe_defaut_explication' => 'Saisir la taxe par défaut applicable aux produits sous la forme 0.20 pour une TVA à 20% par exemple (cette valeur peut être surchargée ensuite pour chaque produit).',
	'configurer_taxe_defaut_label' => 'Taxe par défaut',
	'configurer_titre' => 'Configuration des produits',

	// E
	'erreur_parent_inexistant' => 'Ce parent n’existe pas',
	'erreur_parent_sans_id_secteur' => 'Le type de parent « @objet@ » ne convient pas : il ne peut être placé dans un des secteurs autorisés.',
	'erreur_secteurs_autorises' => 'Choisir un parent situé dans un des secteurs autorisés : @secteurs@',

	// I
	'info_1_produit' => 'Un produit',
	'info_aucun_produit' => 'Aucun produit',
	'info_nb_produits' => '@nb@ produits',

	// L
	'legend_dimensions' => 'Conditionnement',

	// P
	'produit_bouton_ajouter' => 'Ajouter un produit',
	'produit_champ_descriptif_label' => 'Court descriptif',
	'produit_champ_hauteur_label' => 'Hauteur (cm)',
	'produit_champ_immateriel_label' => 'Produit dématérialisé (pas de livraison physique)',
	'produit_champ_largeur_label' => 'Largeur (cm)',
	'produit_champ_longueur_label' => 'Longueur (cm)',
	'produit_champ_poids_label' => 'Poids (g)',
	'produit_champ_prix_label' => 'Prix',
	'produit_champ_prix_ht_label' => 'Prix HT',
	'produit_champ_prix_ttc_label' => 'Prix TTC',
	'produit_champ_reference_abbr' => 'Ref.',
	'produit_champ_reference_label' => 'Référence',
	'produit_champ_rubrique_label' => 'Rubrique',
	'produit_champ_parent_label' => 'Dans le contenu :',
	'produit_champ_parent_explication' => 'Sous la forme « objetN », ou entrez les premières lettres d’un titre pour afficher des propositions.',
	'produit_champ_parent_placeholder' => 'rubrique5, article10, breve15…',
	'produit_champ_taxe_explication' => 'Taxe pour ce produit si différente de celle par défaut (@taxe@). Ce champ peut être laissé vide.',
	'produit_champ_taxe_label' => 'Taxe',
	'produit_champ_texte_label' => 'Texte',
	'produit_champ_titre_label' => 'Titre',
	'produit_editer' => 'Éditer le produit',
	'produit_modifier' => 'Modifier le produit :',
	'produit_nouveau' => 'Nouveau produit',
	'produit_numero' => 'Produit n°',
	'produit_reference' => 'Référence "@reference@"',
	'produit_statut' => 'Modifier le statut :',
	'produits_titre' => 'Produits',
	'publier_rubriques_explication' => 'Publier les rubriques qui contiennent des produits publiés ?',
	'publier_rubriques_label' => 'Publier les rubriques ?',
	'publier_rubriques_label_case' => 'Publier les rubriques',

	// T
	'titre_page_configurer_produits' => 'Produits',
	'tous_les_produits' => 'Tous les produits'
);
