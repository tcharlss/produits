<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/produits?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configurer_editer_ttc_explication' => 'Products prices are editable including Taxes and no longer as Gross Prices',
	'configurer_editer_ttc_label' => 'Edit products price including taxes',
	'configurer_precision_ttc_explication' => 'Indicate the rounding precision length after decimal for the price ATI (default is 2)',
	'configurer_precision_ttc_label' => 'Detail about the ATI prices',
	'configurer_taxe_defaut_explication' => 'Enter the default tax applicable to products such as 0.2 for a 20.0% Tax for example (then this value can be overwritten for each product).',
	'configurer_taxe_defaut_label' => 'Default tax',
	'configurer_titre' => 'Configuration of products',

	// I
	'info_1_produit' => 'One product',
	'info_aucun_produit' => 'No product',
	'info_nb_produits' => '@nb@ products',

	// L
	'legend_dimensions' => 'Packaging',
	'limiter_ajout_explication' => 'Restrict adding products to one or more area',
	'limiter_ajout_label' => 'Limit',
	'limiter_secteur_explication' => 'Choose one or several concerned area',
	'limiter_secteur_label' => 'Choice',

	// P
	'produit_bouton_ajouter' => 'Add a product',
	'produit_champ_descriptif_label' => 'Short description',
	'produit_champ_hauteur_label' => 'Height (cm)',
	'produit_champ_immateriel_label' => 'Dematerialized product (no physical delivery)', # RELIRE
	'produit_champ_largeur_label' => 'Width (cm)',
	'produit_champ_longueur_label' => 'Length (cm)',
	'produit_champ_poids_label' => 'Weight (g)',
	'produit_champ_prix_ht_label' => 'Gross Price',
	'produit_champ_prix_ttc_label' => 'ATI Price',
	'produit_champ_reference_abbr' => 'Ref.',
	'produit_champ_reference_label' => 'Reference',
	'produit_champ_rubrique_label' => 'Section',
	'produit_champ_taxe_explication' => 'Default value on this site: @taxe@. You can leave this field empty.', # MODIF
	'produit_champ_taxe_label' => 'Tax',
	'produit_champ_texte_label' => 'Text',
	'produit_champ_titre_label' => 'Title',
	'produit_editer' => 'Edit this product',
	'produit_modifier' => 'Modify the product :',
	'produit_nouveau' => 'New product',
	'produit_numero' => 'Product n°',
	'produit_reference' => 'Reference "@reference@"',
	'produit_statut' => 'Modify the status :',
	'produits_titre' => 'Products',

	// S
	'secteurs_autorises' => 'Choose a section from one of the authorized areas: @secteurs@',

	// T
	'titre_page_configurer_produits' => 'Products',
	'tous_les_produits' => 'All the products'
);
