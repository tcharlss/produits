<?php
/**
 * Définit les autorisations du plugin produits
 *
 * @plugin     produits
 * @copyright  2014
 * @author     Les Développements Durables, http://www.ldd.fr
 * @licence    GNU/GPL
 * @package    SPIP\Produits\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Afficher une taxe
 *
 * @param float $taxe
 *     Nombre entre 0 et 1
 * @return string
 */
function filtre_produits_afficher_taxe_dist($taxe) {
	if (is_numeric($taxe)) {
		$taxe *= 100;
		$taxe .= '&#37';
	}
	return $taxe;
}


/**
 * Convertit un montant HT vers TTC
 *
 * Sans taxe, c'est celle par défaut qui est utilisée.
 * Idem pour l'arrondi.
 *
 * @param float $montant
 * @param float|string $taxe
 * @param int|string $arrondi
 * @return float
 */
function produits_montant_ht_vers_ttc($montant_ht, $taxe = '', $arrondi = '') {

	include_spip('inc/config');

	$montant_ttc = $montant_ht; // en cas d'erreur on retourne la valeur initiale
	if ($montant_ht = floatval($montant_ht)) {

		// Si la taxe n'est pas donnée, on prend celle par défaut
		if (!is_numeric($taxe)) {
			$taxe = floatval(lire_config('produits/taxe', 0));
		} else {
			$taxe = floatval($taxe);
		}

		// Arrondi
		if (!is_numeric($arrondi)) {
			$arrondi = intval(lire_config('produits/precision_ttc', 2));
		} else {
			$arrondi = intval($arrondi);
		}

		$montant_ttc = round($montant_ht * (1 + $taxe), $arrondi);
	}

	return $montant_ttc;
}


/**
 * Convertit un montant TTC vers HT
 *
 * Sans taxe, c'est celle par défaut qui est utilisée.
 * Idem pour l'arrondi.
 *
 * @param float $montant
 * @param float|string $taxe
 * @param int|string $arrondi
 * @return float
 */
function produits_montant_ttc_vers_ht($montant_ttc, $taxe = '', $arrondi = '') {

	include_spip('inc/config');

	$montant_ht = $montant_ttc; // en cas d'erreur on retourne la valeur initiale
	if ($montant_ttc = floatval($montant_ttc)) {

		// Si la taxe n'est pas donnée, on prend celle par défaut
		if (!is_numeric($taxe)) {
			$taxe = floatval(lire_config('produits/taxe', 0));
		} else {
			$taxe = floatval($taxe);
		}

		// Arrondi
		if (!is_numeric($arrondi)) {
			$arrondi = intval(lire_config('produits/precision_ttc', 2));
		} else {
			$arrondi = intval($arrondi);
		}

		$montant_ht = round($montant_ttc / (1 + $taxe), $arrondi);
	}

	return $montant_ht;
}


/**
 * Retourne le type et le numéro du parent à partir de son alias
 *
 * Vérifie que le type de parent existe,
 * optionnellement en allant vérifier l'objet précis dans la base.
 *
 * @param string $parent
 *     Alias d'un parent sous la forme `objetN` : `article5`, `rubrique10`...
 * @param bool $verifier_base
 *     `true` pour vérifier que l'objet existe en base
 * @return array|false
 *     Tableau [objet,id_objet] ou false sinon
 */
function produits_decoder_parent($parent, $verifier_base = true) {

	static $parents = array();
	$hash = "$parent-$verifier_base";
	if (isset($parents[$hash])) {
		return $parents[$hash];
	}

	$objet_parent = false;
	if (preg_match('/^(?P<objet>[a-zA-Z]+)(?P<id_objet>\d+)$/', $parent, $matches)) {
		include_spip('base/objets');
		$trouver_table = charger_fonction('trouver_table', 'base');
		$objet         = $matches['objet'];
		$id_objet      = intval($matches['id_objet']);
		$table_objet   = table_objet_sql($objet);
		$cle_objet     = id_table_objet($objet);
		$desc_objet    = $trouver_table($table_objet);
		if ($desc_objet and !empty($desc_objet['field'])) {
			$en_base = $verifier_base ? sql_countsel($table_objet, $cle_objet.'='.intval($id_objet)) : true;
			if ($en_base) {
				$objet_parent = array($objet, $id_objet);
			}
		}
	}
	$parents[$hash] = $objet_parent;

	return $objet_parent;
}